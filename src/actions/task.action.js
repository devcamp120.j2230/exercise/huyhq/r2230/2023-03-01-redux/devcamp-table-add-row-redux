import { TASK_BUTTON_CLICK, TASK_INPUT_CHANGE } from "../constant/task.constant";

//mô tả sự kiện khi Input được nhập
const inputChangeHandler = (inputValue)=>{
    return {
        type: TASK_INPUT_CHANGE,
        payload: inputValue
    };
};

//sự kiện khi Button được click
const buttonClickHandler = ()=>{
    return {
        type: TASK_BUTTON_CLICK,
    };
}

export {
    inputChangeHandler,
    buttonClickHandler,
}