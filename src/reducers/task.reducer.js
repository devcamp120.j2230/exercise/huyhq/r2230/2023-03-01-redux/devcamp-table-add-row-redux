import { TASK_BUTTON_CLICK, TASK_INPUT_CHANGE } from "../constant/task.constant";

//các state sẽ thay đổi 
const taskState = {
    inputValue: "",
    tableList: []
};

const taskReducer = (state = taskState, action) => {
    switch (action.type) {
        case TASK_INPUT_CHANGE:
            state.inputValue = action.payload;
            break;
        case TASK_BUTTON_CLICK:
            state.tableList.push({
                tableValue: state.inputValue,
            });
            state.inputValue = "";
            break;
        default:
            break;
    }
    return {...state};
};

export default taskReducer;