import { Button, Container, Grid, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { buttonClickHandler, inputChangeHandler } from "../../actions/task.action";

const TableList = () => {
    //cho phép lấy giá trị từ view đưa về reducer
    const dispatch = useDispatch();
    
    //lấy giá trị từ reducer
    const { inputValue, tableList } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    });

    const onInputTextChange = (event) => {
        dispatch(inputChangeHandler(event.target.value));
    };

    const onButtonClickSend = ()=>{
        dispatch(buttonClickHandler());
    };

    return (
        <Container>
            <Grid container mt={5} alignItems="center" py={5}>
                <Grid item xs={12} md={6} lg={8}>
                    <TextField label="Nhap noi dung ..." variant="outlined" fullWidth onChange={onInputTextChange} value={inputValue}/>
                </Grid>
                <Grid item xs={12} md={6} lg={4} textAlign="center">
                    <Button variant="contained" onClick={onButtonClickSend}>Them</Button>
                </Grid>
            </Grid>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Stt</TableCell>
                        <TableCell>Noi dung</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {tableList.map((value, i)=>{
                        return <TableRow key={i}>
                                <TableCell>{i+1}</TableCell>
                                <TableCell>{value.tableValue}</TableCell>
                            </TableRow>
                    })}
                </TableBody>
            </Table>
        </Container>
    );
};

export default TableList;